from datetime import datetime
from django.db import models


class Game(models.Model):
    date = models.DateTimeField(default=datetime.now, blank=True)
    number_of_players = models.IntegerField(default=1)
    number_of_rounds = models.IntegerField(default=1)


class Player(models.Model):
    name = models.CharField(max_length=200, default="")
    number = models.IntegerField(default=0)
    total_score = models.IntegerField(default=0)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('number',)

class Score(models.Model):
    round = models.IntegerField(default=0)
    cards = models.IntegerField(default=0)
    score = models.IntegerField(default=0)
    tiebreak = models.BooleanField(default=False)
    tiebreak2 = models.BooleanField(default=False)
    minigame = models.IntegerField(default=0)
    player = models.ForeignKey(Player, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return str(self.round) + "," + str(self.cards) + "," + str(self.score) + "," + str(self.tiebreak)

