from django.conf.urls import url
from . import views

app_name = 'dobble'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^players/$', views.players, name='players'),
    url(r'^addPlayer/$', views.addPlayer, name='addPlayer'),
    url(r'^removePlayer/$', views.removePlayer, name='removePlayer'),
    url(r'^addRound/$', views.addRound, name='addRound'),
    url(r'^removeRound/$', views.subtractRound, name='subtractRound'),
    url(r'^reset/$', views.reset, name='reset'),
    url(r'^savePlayers/$', views.savePlayers, name='savePlayers'),
    url(r'^game/$', views.game, name='game'),
    url(r'^updateScores/$', views.updateScores, name='updateScores'),
]
