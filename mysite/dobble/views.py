from django.db.models import Max, Min, Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .models import Game, Player, Score


# Create your views here.
def index(request):
    players = request.GET.get('players', '')
    if players == '':
        players = 1
    print(players)
    context = {
        'players': range(1, int(players) + 1),
        'title': 'All',
    }
    return render(request, 'dobble/players.html', context)


def players(request):
    game = Game.objects.last()
    if game == None:
        game = Game.objects.create()
        game.save()
    number_of_players = game.number_of_players
    players = [x.name for x in game.player_set.all().order_by('number')]
    playerrange = range(1, number_of_players + 1)
    for x in playerrange:
        try:
            players[x - 1]
        except IndexError:
            players.append('')

    print(players)
    context = {
        'range': playerrange,
        'players': players,
        'game': game,
    }
    return render(request, 'dobble/players.html', context)


def addPlayer(request):
    game = Game.objects.last()
    number_of_players = game.number_of_players
    game.number_of_players = number_of_players + 1
    game.save()
    return HttpResponseRedirect(reverse('dobble:players'))


def removePlayer(request):
    game = Game.objects.last()
    number_of_players = game.number_of_players
    game.number_of_players = number_of_players - 1
    game.save()
    return HttpResponseRedirect(reverse('dobble:players'))


def addRound(request):
    game = Game.objects.last()
    number_of_rounds = game.number_of_rounds
    game.number_of_rounds = number_of_rounds + 1
    game.save()
    return HttpResponseRedirect(reverse('dobble:players'))


def subtractRound(request):
    game = Game.objects.last()
    number_of_rounds = game.number_of_rounds
    game.number_of_rounds = number_of_rounds - 1
    game.save()
    return HttpResponseRedirect(reverse('dobble:players'))


def savePlayers(request):
    game = Game.objects.last()
    number_of_players = game.number_of_players
    for x in range(1, number_of_players + 1):
        try:
            p = game.player_set.all()[x - 1]
            print(p)
            if p.name != request.POST['name' + str(x)]:
                p.name = request.POST['name' + str(x)]
                p.number = x
                p.save()
        except IndexError:
            p = Player(name=request.POST['name' + str(x)])
            p.number = x
            p.save()
            game.player_set.add(p)
            game.save()
        for y in range(1, game.number_of_rounds + 1):
            s, created = Score.objects.get_or_create(player=p, round=y)
            s.save()
    return HttpResponseRedirect(reverse('dobble:players'))


def game(request):
    game = Game.objects.last()
    if game == None:
        game = Game.objects.create()
        game.save()

    players = game.player_set.all().order_by('number')
    print(players)
    playerrange = range(1, game.number_of_players + 1)
    roundrange = range(1, game.number_of_rounds + 1)
    scores = []
    for x in roundrange:
        temp = [score for score in Score.objects.filter(round=x)]
        scores.append(temp)
        # print(temp)

    print(scores)
    context = {
        'playerrange': playerrange,
        'roundrange': roundrange,
        'players': players,
        'game': game,
        'scores': scores,
    }
    return render(request, 'dobble/game.html', context)


def updateRound(round):
    scores = Score.objects.filter(round=round)
    minigame = scores[0].minigame
    # print(scores)
    # print(scores.aggregate(Max('cards')))
    # print(minigame)
    if minigame == 1:
        for s in scores:
            s.score = s.cards
            s.save()
        max = scores.aggregate(Max('cards'))['cards__max']
        winners = scores.filter(cards=max)
        if len(winners) == 1:
            winners[0].score += 5
            winners[0].save()
        else:
            winners = winners.filter(tiebreak=True)
            if len(winners) == 1:
                winners[0].score += 5
                winners[0].save()
    elif minigame == 2:
        min = scores.aggregate(Min('cards'))['cards__min']
        winners = scores.filter(cards=min)
        if len(winners) == 1:
            winners[0].score = 10
            winners[0].save()

        max = scores.aggregate(Max('cards'))['cards__max']
        losers = scores.filter(cards=max)
        if len(losers) == 1:
            losers[0].score = -20
            losers[0].save()
    elif minigame == 3:
        for s in scores:
            s.score = 0
            s.save()
        min = scores.aggregate(Min('cards'))['cards__min']
        lowest = scores.filter(cards=min)
        other = scores.filter(cards__gt=min)
        if len(lowest) == 1:
            lowest[0].score = 20
            lowest[0].save()
            min = other.aggregate(Min('cards'))['cards__min']
            second_lowest = other.filter(cards=min)
            if len(second_lowest) == 1:
                second_lowest[0].score = 10
                second_lowest[0].save()
            else:
                second_lowest = second_lowest.filter(tiebreak=True)
                if len(second_lowest) == 1:
                    second_lowest[0].score = 10
                    second_lowest[0].save()
        elif len(lowest) == 2:
            winner = lowest.filter(tiebreak=True)
            if len(winner) == 1:
                winner[0].score = 20
                winner[0].save()

            second = lowest.filter(tiebreak=False)
            if len(second) == 1:
                second[0].score = 10
                second[0].save()
        else:
            winner = lowest.filter(tiebreak=True)
            if len(winner) == 1:
                winner[0].score = 20
                winner[0].save()

            second = lowest.filter(tiebreak2=True)
            if len(second) == 1:
                second[0].score = 10
                second[0].save()
    elif minigame == 4:
        for s in scores:
            s.score = s.cards * -5
            s.save()
    elif minigame == 5:
        for s in scores:
            s.score = s.cards
            s.save()


def updateScores(request):
    game = Game.objects.last()
    number_of_players = game.number_of_players
    number_of_rounds = game.number_of_rounds
    players = game.player_set.all().order_by('number')
    for x in range(1, number_of_players + 1):
        p = players[x - 1]
        for y in range(1, number_of_rounds + 1):
            s, created = Score.objects.get_or_create(player=p, round=y)
            cards_request = request.POST[p.name + ',' + str(y)].split('*')
            if len(cards_request) == 2:
                s.tiebreak = True
            else:
                s.tiebreak = False

            if len(cards_request) > 2:
                s.tiebreak2 = True
            else:
                s.tiebreak2 = False
            s.cards = int(cards_request[0])
            s.minigame = int(request.POST['Minigame' + str(y)])
            s.save()
            p.score_set.add(s)
            p.save()

    for round in range(1, number_of_rounds + 1):
        updateRound(round)

    for x in range(1, number_of_players + 1):
        p = players[x - 1]
        p.total_score = p.score_set.all().aggregate(Sum('score'))['score__sum']
        p.save()
    return HttpResponseRedirect(reverse('dobble:game'))


def reset(request):
    for score in Score.objects.all():
        score.delete()
    for game in Game.objects.all():
        game.delete()

    return HttpResponseRedirect(reverse('dobble:game'))
