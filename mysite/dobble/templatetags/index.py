from django import template

register = template.Library()

@register.filter
def index(List, i):
    return List[int(i)]

@register.filter
def name(List, i):
    return List[int(i)].name

@register.filter
def number(List, i):
    return List[int(i)].number

@register.filter
def total(List, x):
    return List[int(x)].total_score