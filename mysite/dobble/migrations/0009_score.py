# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-09 18:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dobble', '0008_auto_20170109_1527'),
    ]

    operations = [
        migrations.CreateModel(
            name='Score',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('round', models.IntegerField(default=0)),
                ('cards', models.IntegerField(default=0)),
                ('score', models.IntegerField(default=0)),
                ('player', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dobble.Player')),
            ],
        ),
    ]
