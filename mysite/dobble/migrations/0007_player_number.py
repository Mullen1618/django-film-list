# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-09 15:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dobble', '0006_game_number_of_players'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='number',
            field=models.IntegerField(default=0),
        ),
    ]
