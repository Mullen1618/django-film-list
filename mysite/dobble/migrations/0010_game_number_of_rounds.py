# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-09 18:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dobble', '0009_score'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='number_of_rounds',
            field=models.IntegerField(default=1),
        ),
    ]
