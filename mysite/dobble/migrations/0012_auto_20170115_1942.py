# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-15 19:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dobble', '0011_score_minigame'),
    ]

    operations = [
        migrations.AddField(
            model_name='score',
            name='tiebreak',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='player',
            name='name',
            field=models.CharField(default='', max_length=200),
        ),
    ]
