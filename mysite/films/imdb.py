from bs4 import BeautifulSoup
import urllib3

http = urllib3.PoolManager()


def getFilmName(imdb_id):
    r = http.request('GET', 'http://www.imdb.com/title/' + imdb_id)
    html = r.data.decode('utf-8')
    try:
        name = BeautifulSoup(html, "html.parser").find('div', attrs={'class': 'title_wrapper'}).h1.contents[0].strip()
    except AttributeError:
        name = BeautifulSoup(html, "html.parser").find('span', attrs={'class': 'itemprop'}).text.strip()
    return name

def getIMDBid(name):
    r = http.request('GET', 'http://www.imdb.com/find?ref_=nv_sr_fn&q=' + name.replace(' ', '+'))
    html = r.data.decode('utf-8')
    soup = BeautifulSoup(html, "html.parser")
    parsed_html = soup.find('td', attrs={'class': 'result_text'}).a
    imdb_id = str(parsed_html).split('"/title/')[1].split('/?ref')[0]
    return imdb_id

def updateFilmInfo(film):
    if (film.imdb_id == ''):
        film.imdb_id = getIMDBid(film.name)
        film.save()

    r = http.request('GET', 'http://www.imdb.com/title/' + film.imdb_id)
    html = r.data.decode('utf-8')
    try:
        name = BeautifulSoup(html, "html.parser").find('div', attrs={'class': 'title_wrapper'}).h1.contents[0].strip()
    except AttributeError:
        name = BeautifulSoup(html, "html.parser").find('span', attrs={'class': 'itemprop'}).text.strip()
    soup = BeautifulSoup(html,"html.parser")
    try:
        year = soup.find('span', attrs={'id': 'titleYear'}).a.text
    except AttributeError:
        year = soup.find('span', attrs={'class': 'nobr'}).a.text

    try:
        subtext = soup.find('div', attrs={'class': 'subtext'})
        runtime = film.convertHoursAndMinutesToMinutes(subtext.time.text.strip())
    except AttributeError:
        subtext = soup.find('div', attrs={'class': 'infobar'})
        runtime = film.convertHoursAndMinutesToMinutes(subtext.time.text.strip())

    genre = ", ".join([x.text for x in subtext.find_all('span', attrs={'itemprop': 'genre'})])
    film.name = name
    film.year = year
    film.runtime = runtime
    film.genre = genre
    film.save()