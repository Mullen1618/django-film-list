from django.db import models

class Film(models.Model):
    class Meta:
        ordering = ('name',)
    name = models.CharField(max_length=200)
    genre = models.CharField(max_length=200, default='', blank=True)
    year = models.IntegerField(default=0)
    runtime = models.IntegerField(default=0)
    imdb_id = models.TextField(default='', blank=True)

    def convertHoursAndMinutesToMinutes(self, duration):
        split_hours = duration.split('h')
        if len(split_hours) == 1:
            hours = 0
            split_minutes = split_hours[0].split('min')
        else:
            hours = split_hours[0].strip()
            split_minutes = split_hours[1].split('min')

        minutes = split_minutes[0].strip()
        runtime = int(hours) * 60 + int(minutes)
        return runtime

    def __str__(self):
        return str(self.id) + ": " + self.name

    class Meta:
        ordering = ('name',)

class UserFilm(models.Model):
    film = models.ForeignKey(Film)
    user_id = models.TextField(default='')
    watched = models.BooleanField(default=False)

    def __str__(self):
        return self.user_id + ": " + str(self.film) + ": " + self.film.name
