from django.contrib import admin

from .models import Film,UserFilm

admin.site.register(Film)
admin.site.register(UserFilm)
