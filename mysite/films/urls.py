from django.conf.urls import url
from . import views

app_name = 'films'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^watched/$', views.watched, name='watched'),
    url(r'^unwatched/$', views.unwatched, name='unwatched'),
    url(r'^$', views.index, name='all'),
    url(r'^addfilm/$', views.addFilm, name='addfilm'),
    url(r'^addfilm/(?P<notice>[^\/]+)', views.addFilm, name='addfilm'),
    url(r'^add/$', views.add, name='add'),
    url(r'^toggleWatched/$', views.toggleWatched, name='toggleWatched'),
    url(r'^updateFilm/$', views.updateFilm, name='updateFilm'),
    url(r'^deleteFilm/$', views.deleteFilm, name='deleteFilm'),
]
