from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .models import Film, UserFilm
from .imdb import getFilmName, updateFilmInfo, getIMDBid


@login_required(login_url='/login/')
def index(request):
    userFilms = UserFilm.objects.filter(user_id=request.user.id).order_by('film')
    context = {
        'title': 'All',
        'films': userFilms,
    }
    return render(request, 'films/watched_unwatched.html', context)


@login_required(login_url='/login/')
def watched(request):
    userFilms = UserFilm.objects.filter(watched=True, user_id=request.user.id).order_by('film')
    context = {
        'title': 'Watched',
        'films': userFilms,
    }
    return render(request, 'films/watched_unwatched.html', context)


@login_required(login_url='/login/')
def unwatched(request):
    userFilms = UserFilm.objects.filter(watched=False, user_id=request.user.id).order_by('film')

    context = {
        'title': 'Unwatched',
        'films': userFilms,
    }
    return render(request, 'films/watched_unwatched.html', context)


@login_required(login_url='/login/')
def addFilm(request, notice=None):
    name = request.GET.get('name', '')
    watched = request.GET.get('watched', '')
    imdb_id = request.GET.get('imdb', '')
    if imdb_id != '':
        name = getFilmName(imdb_id)
    if watched != "True":
        watched = ''

    context = {
        'title': 'Watched',
        'notice': notice,
        'name': name,
        'watched': watched,
        'imdb_id': imdb_id,
    }
    return render(request, 'films/add_film.html', context)


@login_required(login_url='/login/')
def add(request):
    try:
        if request.POST['filmwatched'] == 'on':
            watched_film = True
        else:
            watched_film = False
    except (KeyError):
        watched_film = False
    imdb = request.POST['imdb']
    if imdb == '':
        imdb = getIMDBid(request.POST['name'])

    if len(Film.objects.filter(imdb_id=imdb)) == 0:
        f = Film(imdb_id=imdb)
        f.save()
        updateFilmInfo(f)
        print(Film.objects.all())
    else:
        f = Film.objects.get(imdb_id=imdb)  # Will throw an error if there are multiple films

    user_id = request.user.id

    if len(f.userfilm_set.filter(user_id=user_id)) == 0:
        f.userfilm_set.create(user_id=request.user.id, watched=watched_film)
        message = request.POST['name'] + " has been added!"
    else:
        message = "Film already exists and was not added!"
        print("Film already in users list")

    print(Film.objects.all())
    print(f.userfilm_set.all())

    return HttpResponseRedirect(reverse('films:addfilm', args=(message,)))


@login_required(login_url='/login/')
def toggleWatched(request):
    print("Toggle")
    film = UserFilm.objects.get(film_id=request.POST['id'], user_id=request.user.id)
    if film.watched == False:
        film.watched = True
    else:
        film.watched = False
    film.save()

    redirect = request.POST['redirect']
    return HttpResponseRedirect(reverse('films:' + redirect.lower()))


@login_required(login_url='/login/')
def updateFilm(request):
    print("Updated")
    film_id = request.POST['id']
    film = Film.objects.get(id=film_id)
    updateFilmInfo(film)

    redirect = request.POST['redirect']
    return HttpResponseRedirect(reverse('films:' + redirect.lower()))


@login_required(login_url='/login/')
def deleteFilm(request):  # When a user is deleted, all the users films need to be too
    film = UserFilm.objects.get(film_id=request.POST['id'], user_id=request.user.id)
    film.delete()

    redirect = request.POST['redirect']
    return HttpResponseRedirect(reverse('films:' + redirect.lower()))
