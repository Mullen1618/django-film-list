// ==UserScript==
// @name         Film List
// @version      0.1
// @description  try to take over the world!
// @author       Conor Mullen
// @grant        none
// ==/UserScript==

function openInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}

(function() {
    'use strict';
    var url = window.location.href;
    var imdb_id = url.split("/")[4]; //Very rough
    openInNewTab("http://127.0.0.1:8000/films/addfilm/"+"?imdb="+imdb_id);
})();